const APIKey="81534b6aebcf833336e6fefdf0bf8f01"
const APIToken="ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A"

function getAllCards(boardId,getCards) {
    try {
      //let url = `https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`;
      let url = `https://api.trello.com/1/boards/${boardId}/lists?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
      let promises=[]
         
      let promiseAllCards= new Promise((resolve, reject) => {
        fetch(url)
          .then((res) => res.json())
          .then((lists)=>{
                lists.forEach(list => {
                    promises.push(getCards(list.id))
                });
                return promises
          })
          .then((promises)=>{
                resolve(Promise.all(promises))
          })
          .catch((err) => reject(err));
      });

      return promiseAllCards;
    } catch (error) {
      console.log(error);
    }
  }
  
  
  module.exports = getAllCards;