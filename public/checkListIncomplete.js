const APIKey = "81534b6aebcf833336e6fefdf0bf8f01";
const APIToken =
  "ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A";

function getCheckItemsId(cardId) {
  let url = `https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${APIToken}`;

  let promise = new Promise((resolve, reject) => {
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        let ids = getIds(json);
        resolve(ids);
      })
      .catch((err) => {
        reject(err);
      });
  });

  return promise;
}

function getIds(json) {
  let ids = json.map((checklist) => {
    let checkItemIds = checklist["checkItems"].map((checkItem) => checkItem.id);
    return checkItemIds;
  });
  return ids.flat(1);
}

function updateCheckItems(cardId, idCheckItem) {
  let promise = new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?key=${APIKey}&token=${APIToken}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          state: "incomplete",
        }),
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((json) => resolve(json))
      .catch((err) => reject(err));
  });
  return promise;
}

getCheckItemsId("6654ad9dd04d80cec4867432")
  .then((ids) => {
    return ids.reduce((promise, id) => {
      return promise.then(() =>
        updateCheckItems("6654ad9dd04d80cec4867432", id)
      );
    }, Promise.resolve());
  })
  .catch((err) => {
    console.log(err);
  });
