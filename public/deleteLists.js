const createBoardListCard = require("./createBoardListCard");

const APIKey = "81534b6aebcf833336e6fefdf0bf8f01";
const APIToken =
  "ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A";



function deleteList(listId) {
  let promise = new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/closed?key=${APIKey}&token=${APIToken}&value=true`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
  return promise;
}

function deleteAllList(lists) {
  return lists.reduce((promise, listId) => {
    return promise.then(() => deleteList(listId));
  }, Promise.resolve());
}

createBoardListCard("newboard-15")
  .then((lists) => {
   return deleteAllList(lists);
  })
  .then((res)=>{
    console.log(res)
  })
  .catch((err) => {
    console.log(err);
  });
