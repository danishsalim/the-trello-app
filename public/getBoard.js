function getBoard(boardId) {
  let promise = new Promise((resolve, reject) => {
    let board = fetch(
      `https://api.trello.com/1/boards/${boardId}?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`
    )
      .then((res) => res.json())
      .then((json) => resolve(json))
      .catch((err) => reject(err));
  });
  return promise;
}

module.exports = getBoard;
