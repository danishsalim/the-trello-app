const createBoard = require("./createBoard");

const APIKey = "81534b6aebcf833336e6fefdf0bf8f01";
const APIToken =
  "ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A";

function createBoardListCard(boardName) {
  let promise = new Promise((resolve, reject) => {
    createBoard(boardName)
      .then((board) => {
        let promises = [];
        for (let index = 1; index < 4; index++) {
          promises.push(createList(board.id, "list" + index));
        }
        return Promise.all(promises);
      })
      .then((lists) => {
        let promises = [];
        let listIds =[]
        for (let list of lists) {
          promises.push(createCard(list.id));
          listIds.push(list.id)
        }
        resolve(listIds);
        return Promise.all(promises);
      })
      .then((cards) => {
        // console.log(cards);
      })
      .catch((err) => reject(err));
  });
  return promise;
}

function createList(boardId, listName) {
  let promise = new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${APIKey}&token=${APIToken}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
  return promise;
}

function createCard(listId) {
  let promise = new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&key=${APIKey}&token=${APIToken}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
        body:{
          name:'card'
        }
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
  return promise;
}

module.exports = createBoardListCard;
