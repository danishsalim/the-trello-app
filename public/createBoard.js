const APIKey = "81534b6aebcf833336e6fefdf0bf8f01";
const APIToken =
  "ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A";

function createBoard(boardName) {
  try {
    let promise = new Promise((resolve, reject) => {
      fetch(
        `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`,
        {
          method: "POST",
        }
      )
        .then((response) => {
          return response.json();
        })
        .then((board) => resolve(board))
        .catch((err) => reject(err));
    });
    return promise;
  } catch (error) {
    console.log(error);
  }
}

module.exports = createBoard;
