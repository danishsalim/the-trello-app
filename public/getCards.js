function getCards(listId) {
    try {
      let url = `https://api.trello.com/1/lists/${listId}/cards?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
         
      let promise = new Promise((resolve, reject) => {
        fetch(url)
          .then((res) => res.json())
          .then((json) => resolve(json))
          .catch((err) => reject(err));
      });
      return promise;
    } catch (error) {
      console.log(error);
    }
  }
  
  
  module.exports = getCards;